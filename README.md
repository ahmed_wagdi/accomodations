## Run instructions
1. `git clone git@bitbucket.org:ahmed_wagdi/accomodations.git`
2. Go into the project directory
3. cd client
4. Run `npm install`
5. Run `npm start`

To make the project easier to run I uploaded the backend server to heroku so you don't need to set it up locally. But if you want to run both the backend and the frontend locally then in addition to the above steps do the following:

1. Go to client/src/utils.js
2. Remove the `API_URL` pointing to the heroku app and uncomment the other one.
3. Go to project root
4. Run `npm install`
5. Run `npm start`

## Libraries/Tools used:
**Backend:**

1. Node
2. Express

**Frontend:**

1. React
2. Redux
3. Redux Thunk
4. Momentjs
5. Create react app