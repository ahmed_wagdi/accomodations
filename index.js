const express = require('express')
const app = express()
const ratings = require('./reviews.json');
var bodyParser = require('body-parser');

ratings.sort((a, b) => b.entryDate - a.entryDate)
// Add headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

var port = process.env.PORT || 8080;

const ratings_count = ratings.length;
const current_date = new Date().getTime();
/*
Responsible for calculating Rating Weight
*/
function calculateWeight(rating){
    const age = (current_date - rating.entryDate) / 31536000000;
    if(age > 5) return 0.5;

    return 1 - (age * 0.1);
}

/* 
Responsible for taking an array of ratings
and returning the averages.
Formula used is:
(cummulative ratings * weight) / total_of_weights
*/
function calculateAverages(ratings){
    let general_average = 0;
    const aspects_average = {};
    const count = ratings.length;
    let weights = 0;
    for(let i = 0; i < count; i++){
        const weight = calculateWeight(ratings[i]);
        general_average += ratings[i].ratings.general.general * weight;
        weights += weight;
        const aspects = ratings[i].ratings.aspects;

        /* 
        Loop over ratings.aspects and increment the rating/count
        for each aspect but only if rating is greater than 0
        */
        for (var key in aspects){
            if(aspects[key] > 0){
                /* 
                If first rating intialize the values first
                */
                if(!aspects_average[key]){
                    aspects_average[key] = {
                        total: 0,
                        weights: 0
                    };
                }
                aspects_average[key].total += aspects[key] * weight;
                aspects_average[key].weights += weight;
            }
        }
    }

    for (var key in aspects_average){
        aspects_average[key] = aspects_average[key].total / aspects_average[key].weights;
        aspects_average[key] = Math.round( aspects_average[key] * 10 ) / 10
    }

    general_average = general_average / weights;
    general_average = Math.round( general_average * 10 ) / 10

    return {general_average, aspects_average};
}

app.get('/', function (req, res) {
    const data = calculateAverages();
    res.status(200).json(data);
})

function sortRatings(ratings, sort_by){
    ratings.sort((a, b) => b[sort_by] - a[sort_by]);
}

app.get('/ratings', function (req, res) {
    const data = { paginator: {} }
    const limit = 20;
    const {filter, sort} = req.query;
    const page = parseInt(req.query.page) || 1; 
    data.results = [...ratings];
    if(filter){
        data.results = data.results.filter(rating => rating.traveledWith === filter);
    }

    if(sort){
        sortRatings(data.results, sort);
    }

    if(page === 1){
        data.averages = calculateAverages(data.results);
    }

    data.paginator = {
        previous_page : page > 1 ? page-1 : null,
        next_page : page < data.results.length / limit ? page+1 : null,
        current_page: page
    }

    data.results = data.results.slice(((page-1)*limit), (page*limit));
    res.status(200).json(data);
})

app.listen(port, function () {
  console.log('Example app listening on port ' + port)
})