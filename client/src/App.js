import React, { Component } from 'react';
import './App.css';

import Averages from './components/Averages';
import RatingList from './components/RatingList';
import Nav from './components/Nav';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Nav />
        <div className="main">
          <Averages />
          <RatingList />
        </div>
      </div>
    );
  }
}

export default App;
