export function addQueryString(uri, key, value) {   
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
    if( value === undefined ) {
      if (uri.match(re)) {
          return uri.replace(re, '$1$2');
      } else {
          return uri;
      }
    } else {
      if (uri.match(re)) {
          return uri.replace(re, '$1' + key + "=" + value + '$2');
      } else {
      var hash =  '';
      if( uri.indexOf('#') !== -1 ){
          hash = uri.replace(/.*#/, '#');
          uri = uri.replace(/#.*/, '');
      }
      var separator = uri.indexOf('?') !== -1 ? "&" : "?";    
      return uri + separator + key + "=" + value + hash;
    }
    } 
};

// const API_URL = "/ratings";
const API_URL = "https://accomodations.herokuapp.com/ratings";

export default API_URL;