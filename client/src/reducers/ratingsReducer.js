import {GET_RATINGS, FILTER_RATINGS, SORT_RATINGS} from '../actions';
const initial_state = {
    results: [],
    paginator: {},
    averages: { general: 0, aspects_average: []},
    filter: "",
    sort: "entryDate"
}

/*
Transforms the object containing the aspect rating to an array
to make it easier to render
*/
function formatAspects(aspects_average){
    const new_averages = [];
    for(var key in aspects_average){
        new_averages.push({title: key, rating: aspects_average[key]});
    }
    return new_averages;
}

export default function ratingsReducer(state = initial_state, action){
    switch(action.type){
        case GET_RATINGS:
            const { results, averages, paginator} = action.payload;

            /*
            Loop over ratings and transform the aspects object
            */
            const newResults = results.map(item => {
                item.ratings.aspects = formatAspects(item.ratings.aspects);
                return item;
            })

            const newState = {
                results: newResults, 
                paginator
            };

            if(averages){
                newState.averages = { ...averages, aspects_average: formatAspects(averages.aspects_average) };
                newState.averages.aspects_average.sort((a, b) => a.title > b.title);
            }

            return {
                ...state,
                ...newState
            }
        
        case FILTER_RATINGS:
            return { ...state, filter: action.payload }
        
        case SORT_RATINGS:
            return { ...state, sort: action.payload }

        default:
            return state;
    }
}