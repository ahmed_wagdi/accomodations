import {combineReducers} from 'redux';
import ratingsReducer from './ratingsReducer';

export default combineReducers({
    ratings: ratingsReducer
})