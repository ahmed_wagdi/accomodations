import React from 'react';

export default function CircleProgress(props){
    const radius = 70;
    let val = props.progress;
    var c = Math.PI*(radius*2);
   
    if (!val) { val = 0;}
    if (val > 100) { val = 100;}
    
    var pct = ((100-(val*10))/100)*c;
    
    // $('#cont').attr('data-pct',val);
    return(
        <div className="circle_progress" data-pct={val}>
            <svg width="200" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <circle r={radius} cx="100" cy="100" fill="transparent" strokeDasharray={c} strokeDashoffset="0"></circle>
                <circle className="bar" strokeLinecap="round" style={{strokeDashoffset: pct}} r={radius} cx="100" cy="100" fill="transparent" strokeDasharray={c}></circle>
            </svg>
        </div>
    )
}