import React from 'react';
import moment from 'moment';
import Progress from './Progress';
export default class Rating extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            showDetails: false
        }
    }
    render(){
        const {rating} = this.props;
        const text = rating.texts[rating.locale];
        const title = rating.titles[rating.locale];
        const date = new Date(rating.entryDate);
        const travel_date = moment(rating.travelDate).format("dddd, MMMM Do YYYY");;
        return(
            <li className="rating">
                <div className="rating__head">
                    <h3 className="rating__user">{rating.user}</h3>
                    <span className="rating_score">{rating.ratings.general.general}</span>
                    <span className="rating__travel_date">Traveled on: {travel_date}</span>
                    <div className="rating__entry">Entry Date: {date.getFullYear()}</div>
                </div>
                
                <a className="rating__show" onClick={() => this.setState({showDetails: !this.state.showDetails})}>
                {this.state.showDetails ? "Hide Details" : "Show Details"}
                </a>
                <div className={`aspects_container ${this.state.showDetails ? "open" : ""}`}>
                    <div className="aspect_averages">
                        {rating.ratings.aspects.filter(item => item.rating > 0).map((item, index) => <Progress key={index} progress={item.rating} title={item.title} />)}
                    </div>
                </div>
                {title && <h6 className="rating__title">{title}</h6>}
                <p className="rating__text">{text}</p>
            </li>
        )
    }
}