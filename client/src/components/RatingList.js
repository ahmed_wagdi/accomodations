import React from 'react';
import Rating from './Rating';
import Dropdown from './Dropdown';
import {connect} from "react-redux";
import {getRatings} from '../actions';
import API_URL, {addQueryString} from '../utils';

class RatingList extends React.Component {

  componentDidMount(){
    this.url = API_URL;
    this.props.getRatings(this.url);
  }

  componentWillReceiveProps(newProps){
    if(this.props.filter !== newProps.filter) {
      this.url = addQueryString(this.url, "filter", newProps.filter);
      this.url = addQueryString(this.url, "page", 1);
      this.props.getRatings(this.url);
    }
    if(this.props.sort !== newProps.sort) {
      this.url = addQueryString(this.url, "sort", newProps.sort);
      this.url = addQueryString(this.url, "page", 1);
      this.props.getRatings(this.url);
    }
  }

  paginate(page){
    window.scrollTo( 0, 0 );
    this.url = addQueryString(this.url, "page", page);
    this.props.getRatings(this.url);
  }

  render() {
    const {paginator, ratings} = this.props;
    return (
      <div>

        <ul className="rating_list">
          {ratings.map((rating, index) => <Rating key={index} rating={rating} />)}
        </ul>

        {paginator && <div className="pagination">
          {paginator.previous_page && <div>
            <a className="pagination__link" onClick={() => this.paginate(paginator.previous_page)}><i className="fa fa-angle-left"></i> Previous</a>
          </div>}
          <div className="pagination__current">
            {paginator.current_page}
          </div>
          {paginator.next_page && <div>
            <a className="pagination__link" onClick={() => this.paginate(paginator.next_page)}>Next <i className="fa fa-angle-right"></i> </a>
          </div>}
        </div> } 
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    ratings: state.ratings.results,
    paginator: state.ratings.paginator,
    filter: state.ratings.filter,
    sort: state.ratings.sort
  };
}
export default connect(mapStateToProps, {getRatings})(RatingList);