import React from 'react';

export default function Progress(props){
    const progress = props.progress * 10 + "%";
    return(
        <div className="progress">
            <h6 className="progress__label">{props.title}: <span className="rating_score">{props.progress}</span></h6>
            <div className="progress__bar">
                <div className="progress__completed" style={{width: progress}}></div>
            </div>
        </div>
    )
}