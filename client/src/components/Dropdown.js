import React from 'react';

export default class Dropdown extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            open: false,
        }
        this.toggle = this.toggle.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.options = {};
        React.Children.map(this.props.children, (child) => (
            this.options[child.props.value] = child.props.children
        ))
    }

    handleClick(e){
        if(e.target !== this.refs.dropdown) this.setState({open: false});
    }

    componentDidMount(){
        document.addEventListener("click", this.handleClick);
    }

    componentWillUnmount(){
        document.removeEventListener("click", this.handleClick);
    }

    toggle(){
        this.setState({open: !this.state.open});
    }

    select(e){
        if(typeof this.props.onSelect === "function") this.props.onSelect(e.target.id);
    }

    render(){
        const head = this.options[this.props.selected];
        return(
            <div className="dropdown">
                <div className="dropdown__label">{this.props.head}:</div>
                <a ref="dropdown" className="dropdown__head" onClick={this.toggle}>
                    {head}
                   <i className="fa fa-angle-down"></i> 
                </a>

                <ul className={`dropdown__list ${this.state.open && "dropdown__list--open"}`}>
                    {
                        React.Children.map(this.props.children, (child) => (
                            <li>
                                <a onClick={(e) => this.select(e)} className={`dropdown__item ${child.props.value === this.props.selected ? "active" : ""}`} id={child.props.value}>{child.props.children}</a>
                            </li>
                        ))
                    }
                </ul>
            </div>      
        )
    }
}