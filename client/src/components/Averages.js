import React from 'react';
import CircleProgress from './CircleProgress';
import Progress from './Progress';

import {connect} from 'react-redux';

function Averages(props){
    return (
        <div className="averages">
            <div className="general">
                <h2>General Rating</h2>
                <CircleProgress progress={props.general} />
            </div>
            
            <div className="aspect_averages">
                {props.aspects.map((item, index) => <Progress key={index} progress={item.rating} title={item.title} />)}
            </div>
        </div>
    )
}

function mapStateToProps(state){
    return{
        general: state.ratings.averages.general_average,
        aspects: state.ratings.averages.aspects_average,
    }
}
export default connect(mapStateToProps)(Averages);