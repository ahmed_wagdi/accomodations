import React from 'react';
import {connect} from 'react-redux';
import Dropdown from './Dropdown';
import {filterRatings, sortRatings} from '../actions';

class Nav extends React.Component{
    filter(selected){
        this.props.filterRatings(selected);
    }
    
    sort(sort_by){
        this.props.sortRatings(sort_by);
    }
    
    render(){
        return(
            <nav className="ratings_head">  
                <h1>Reviews</h1>
                <div className="dropdown_container">
                    <Dropdown head="Filter" selected={this.props.filter} onSelect={(selected) => this.filter(selected)}>
                        <option value="">All</option>
                        <option value="FAMILY">Family</option>
                        <option value="COUPLE">Couple</option>
                        <option value="SINGLE">Single</option>
                        <option value="FRIENDS">Friends</option>
                        <option value="OTHER">Other</option>
                    </Dropdown>
        
                    <Dropdown head="Sort by" selected={this.props.sort} onSelect={(selected) => this.sort(selected)}>
                        <option value="travelDate">Travel Date</option>
                        <option value="entryDate">Review Date</option>
                    </Dropdown>
                </div>
            </nav>
        )
    }
}

function mapStateToProps(state){
    return {
        filter: state.ratings.filter,
        sort: state.ratings.sort
    }
}
export default connect(mapStateToProps, {filterRatings, sortRatings})(Nav);