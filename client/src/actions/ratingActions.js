import {GET_RATINGS, FILTER_RATINGS, SORT_RATINGS} from './types';

export function getRatings(url){
    return function(dispatch){
        fetch(url).then(response => response.json()).then(response => {
            dispatch({type: GET_RATINGS, payload: response})
        })
    }
}

export function filterRatings(filter){
    return {
        type: FILTER_RATINGS,
        payload: filter
    }
}

export function sortRatings(order){
    return {
        type: SORT_RATINGS,
        payload: order
    }
}